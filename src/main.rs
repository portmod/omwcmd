extern crate crossterm;
extern crate crossterm_input;
extern crate esplugin;
extern crate tui;
extern crate walkdir;
#[macro_use]
extern crate clap;

mod conflicts;
mod dds;

use clap::{App, AppSettings};
use conflicts::conflicts;
use dds::get_dds_dimensions;
use esplugin::{GameId, Plugin};
use std::collections::HashSet;
use std::io;
use std::path::Path;
use walkdir::WalkDir;

fn main() -> Result<(), io::Error> {
    let yaml = load_yaml!("cli.yaml");
    let app = App::from_yaml(yaml).setting(AppSettings::ArgRequiredElseHelp);
    let matches = app.get_matches();

    if let Some(matches) = matches.subcommand_matches("file-conflicts") {
        let mut entries = Vec::new();
        let mut ignore = HashSet::new();
        if let Some(values) = matches.values_of("ignore") {
            for extstr in values {
                for val in extstr.split(",") {
                    ignore.insert(val.to_string().to_lowercase());
                }
            }
        }
        for dir in matches.values_of("DIRS").unwrap() {
            let mut files = HashSet::new();
            for file in WalkDir::new(&dir)
                .into_iter()
                .filter_map(Result::ok)
                .filter(|e| {
                    !e.file_type().is_dir()
                        && e.path()
                            .extension()
                            .map(|ext| !ignore.contains(&ext.to_str().unwrap_or("").to_lowercase()))
                            .unwrap_or(true)
                })
            {
                if let Ok(suffix) = file.path().strip_prefix(&dir) {
                    files.insert(suffix.display().to_string());
                } else {
                    panic!(
                        "Internal Error: File {} is not in directory {}!",
                        file.path().display(),
                        &dir
                    );
                }
            }
            entries.push((dir.to_string(), files));
        }
        conflicts("Directories", "Files", &entries)?;
        return Ok(());
    }

    if let Some(matches) = matches.subcommand_matches("masters") {
        matches.value_of("MASTER").map(|filename| {
            let mut plugin = Plugin::new(GameId::Morrowind, Path::new(&filename));
            plugin
                .parse_file(true)
                .unwrap_or_else(|e| eprintln!("Error when parsing file: {}", e));
            match plugin.masters() {
                Ok(list) => {
                    if list.len() > 0 {
                        println!("{}", list.join("\n"));
                    }
                }
                Err(e) => {
                    eprintln!("Error: {}", e);
                    std::process::exit(1);
                }
            }
        });
    }

    if let Some(matches) = matches.subcommand_matches("dds") {
        matches
            .value_of("DDS")
            .map(|filename| match get_dds_dimensions(filename.to_string()) {
                Ok((width, height)) => {
                    println!("{} {}", width, height);
                }
                Err(e) => {
                    eprintln!("Error: {}", e);
                    std::process::exit(1);
                }
            });
    }
    Ok(())
}
