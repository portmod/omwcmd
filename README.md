# omwcmd

A command line tool for manipulating files related to OpenMW.

Current Features include:
- Parsing a list of masters from a plugin file (`omwcmd masters`)
- Parsing dimensions from DDS texture files (`omwcmd dds`)
- Displaying conflicts between different directories in the OpenMW VFS (i.e. displaying file conflicts/overrides between mods) (`omwcmd file-conflicts`).
